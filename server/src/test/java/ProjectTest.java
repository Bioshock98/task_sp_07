import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import ru.pyshinskiy.tm.config.PersistenceJPAConfig;
import ru.pyshinskiy.tm.model.Project;
import ru.pyshinskiy.tm.service.project.IProjectService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = { PersistenceJPAConfig.class },
        loader = AnnotationConfigContextLoader.class)
public class ProjectTest {

    @Autowired
    private IProjectService projectService;

    @Test
    public void projectCreate() {
        @NotNull final Project project = new Project();
        projectService.save(project);
    }
}
