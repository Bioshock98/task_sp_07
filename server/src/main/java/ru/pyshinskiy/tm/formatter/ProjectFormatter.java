package ru.pyshinskiy.tm.formatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.model.Project;
import ru.pyshinskiy.tm.service.project.IProjectService;

import java.util.Locale;

@Component
public class ProjectFormatter implements Formatter<Project> {

    @Autowired
    private IProjectService projectService;

    @Override
    public Project parse(String projectId, Locale locale) {
        return projectService.findOne(projectId);
    }

    @Override
    public String print(Project project, Locale locale) {
        return project.getName();
    }
}
