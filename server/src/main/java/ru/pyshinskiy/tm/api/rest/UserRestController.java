package ru.pyshinskiy.tm.api.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.UserDTO;
import ru.pyshinskiy.tm.service.user.IUserService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
@PreAuthorize("hasRole('ADMIN')")
public class UserRestController {

    @Autowired
    private IUserService userService;

    @Autowired
    private DTOConverter dtoConverter;

    @GetMapping("/all")
    public ResponseEntity<List<UserDTO>> getAll() {
        return ResponseEntity.ok(userService.findAllUsersWithRoles().stream().map(e -> dtoConverter.toUserDTO(e)).collect(Collectors.toList()));
    }

    @GetMapping("{id}")
    public ResponseEntity<UserDTO> getUser(@PathVariable("id") @NotNull final String id) {
        return ResponseEntity.ok(dtoConverter.toUserDTO(userService.findOne(id)));
    }

    @PostMapping(value = "/save", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<UserDTO> saveUser(@RequestBody @NotNull final UserDTO userDTO) {
        userService.save(dtoConverter.toUser(userDTO));
        return ResponseEntity.ok(userDTO);
    }

    @PutMapping(value = "/save", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public void updateUser(@RequestBody @NotNull final UserDTO userDTO) {
        userService.save(dtoConverter.toUser(userDTO));
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteUser(@PathVariable("id") @NotNull final String id) {
        userService.remove(id);
    }
}
