package ru.pyshinskiy.tm.api.web;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.UserDTO;
import ru.pyshinskiy.tm.service.user.IUserService;

@Getter
@Setter
@Component
public class AccountController {

    @Autowired
    private IUserService userService;

    @Autowired
    private DTOConverter dtoConverter;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private UserDTO user;

    public String register() {
        user.setPasswordHash(passwordEncoder.encode(user.getPasswordHash()));
        userService.save(dtoConverter.toUser(user));
        return "projects.xhtml?faces-redirect=true";
    }
}
