package ru.pyshinskiy.tm.api.web;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.ProjectDTO;
import ru.pyshinskiy.tm.dto.TaskDTO;
import ru.pyshinskiy.tm.enumerated.Status;
import ru.pyshinskiy.tm.security.UserPrincipal;
import ru.pyshinskiy.tm.service.project.IProjectService;
import ru.pyshinskiy.tm.service.task.ITaskService;
import ru.pyshinskiy.tm.service.user.IUserService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Component
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private DTOConverter dtoConverter;

    @Nullable
    private TaskDTO selectedTask;

    @Nullable
    private TaskDTO editingTask;

    @Nullable
    private TaskDTO addingTask;

    @Nullable
    private TaskDTO removingTask;

    @Nullable
    private List<TaskDTO> filteredTasks;

    @NotNull
    private List<String> statuses = Arrays.stream(Status.values()).map(Enum::toString).collect(Collectors.toList());

    @NotNull
    private String taskAction = "Task add";

    @NotNull
    public TaskDTO getAddingTask() {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        @NotNull final UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        taskDTO.setUserId(userPrincipal.getId());
        return taskDTO;
    }

    public List<TaskDTO> getTasks() {
        @NotNull final UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return taskService.findAllTasksByUserId(userPrincipal.getId())
                .stream()
                .map(e -> dtoConverter.toTaskDTO(e))
                .collect(Collectors.toList());
    }

    public List<ProjectDTO> getProjects() {
        @NotNull final UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return projectService.findAllProjectsByUserId(userPrincipal.getId())
                .stream()
                .map(e -> dtoConverter.toProjectDTO(e))
                .collect(Collectors.toList());
    }

    public String updateTask() {
        taskService.save(dtoConverter.toTask(editingTask));
        return "tasks.xhtml?faces-redirect=true";
    }

    public String deleteTask() {
        taskService.remove(removingTask.getId());
        return "tasks.xhtml?faces-redirect=true";
    }
}
