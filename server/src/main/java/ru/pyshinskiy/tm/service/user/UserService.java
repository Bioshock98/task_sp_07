package ru.pyshinskiy.tm.service.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.model.User;
import ru.pyshinskiy.tm.repository.RoleRepository;
import ru.pyshinskiy.tm.repository.UserRepository;

import java.util.List;

@Service
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    @Nullable
    public User findOne(@Nullable final String id) {
        return userRepository.findUserById(id);
    }

    @Override
    @NotNull
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @NotNull
    public List<User> findAllUsersWithRoles() {
        return userRepository.findAllUsersWithRoles();
    }

    @Transactional
    @Override
    public void save(@Nullable final User user) {
        roleRepository.saveAll(user.getRoles());
        userRepository.save(user);
        user.getRoles().stream().forEach(e -> e.setUser(user));
        roleRepository.saveAll(user.getRoles());
    }

    @Transactional
    @Override
    public void remove(@Nullable final String userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public User findUserByLogin(@NotNull final String login) {
        return userRepository.findUserByLogin(login);
    }
}
