package ru.pyshinskiy.tm.service.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.model.Project;
import ru.pyshinskiy.tm.repository.ProjectRepository;
import ru.pyshinskiy.tm.service.AbstractService;

import java.util.List;

@Service
public class ProjectService extends AbstractService<Project> implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @Nullable
    public Project findOne(@Nullable final String id){
        return projectRepository.findProjectById(id);
    }

    @Override
    @Nullable
    public Project findProjectByUserId(@NotNull final String userId, @NotNull final String id) {
        return projectRepository.findProjectByIdAndUserId(id, userId);
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @NotNull
    public List<Project> findAllProjectsWithTasks() {
        return projectRepository.findAllProjectsWithTasks();
    }

    @Override
    @NotNull
    public List<Project> findAllProjectsByUserId(@NotNull final String userId) {
        return projectRepository.findProjectsByUserId(userId);
    }

    @Transactional
    @Override
    public void save(@Nullable final Project project) {
        projectRepository.save(project);
    }

    @Transactional
    @Override
    public void remove(@Nullable final String projectId) {
        projectRepository.deleteById(projectId);
    }

    @Transactional
    public void removeByUserId(@NotNull final String projectId, @NotNull final String userId) {
        projectRepository.deleteProjectByIdAndUserId(projectId, userId);
    }

}
