package ru.pyshinskiy.tm.service.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.model.User;
import ru.pyshinskiy.tm.service.IService;

import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    List<User> findAllUsersWithRoles();

    @Nullable
    User findUserByLogin(@NotNull final String login);
}
