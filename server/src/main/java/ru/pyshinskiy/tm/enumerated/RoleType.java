package ru.pyshinskiy.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum RoleType {

    ROLE_USER("ROLE_USER"),
    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_TEST("ROLE_TEST");

    @NotNull
    private String name;

    RoleType(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String displayName() {
        return name;
    }
}
