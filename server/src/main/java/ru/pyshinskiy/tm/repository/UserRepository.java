package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.pyshinskiy.tm.model.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Query("select distinct u from User u left join fetch u.roles where u.id = :id")
    @Nullable
    User findUserById(@Param("id") @NotNull final String id);

    @Query("select distinct u from User u left join fetch u.roles where u.login = :login")
    @Nullable
    User findUserByLogin(@Param("login") @NotNull final String login);

    @Query("select distinct u from User u left join fetch u.roles")
    @NotNull
    List<User> findAllUsersWithRoles();
}
