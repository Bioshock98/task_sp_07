package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.pyshinskiy.tm.model.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    @Nullable
    Project findProjectById(@NotNull final String id);

    @NotNull
    @Query("select distinct p from Project p left join fetch p.tasks")
    List<Project> findAllProjectsWithTasks();

    @NotNull
    @Query("select distinct p from Project p left join fetch p.tasks where p.user.id = :userId")
    List<Project> findAllProjectsWithTasksByUserId(@Param("userId") @NotNull final String userId);

    @NotNull
    List<Project> findProjectsByUserId(@NotNull final String userId);

    @Nullable
    Project findProjectByIdAndUserId(@NotNull final String projectId, @NotNull final String userId);

    void deleteProjectByIdAndUserId(@NotNull final String projectId, @NotNull final String userId);
}
