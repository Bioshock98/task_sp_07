package ru.pyshinskiy.tm;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.web.client.HttpClientErrorException;
import ru.pyshinskiy.tm.api.ProjectRestClient;
import ru.pyshinskiy.tm.api.TaskRestClient;
import ru.pyshinskiy.tm.api.UserRestClient;
import ru.pyshinskiy.tm.config.ApplicationConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = { ApplicationConfig.class },
        loader = AnnotationConfigContextLoader.class)
@Category(ru.pyshinskiy.tm.IntegrateRestClientTest.class)
public class SecurityRestClientTest {

    @Autowired
    private ProjectRestClient projectRestClient;

    @Autowired
    private TaskRestClient taskRestClient;

    @Autowired
    private UserRestClient userRestClient;

    @Test(expected = HttpClientErrorException.Unauthorized.class)
    public void doUnauthorizedRequestToProjectService() {
        projectRestClient.getAllProjects();
    }

    @Test(expected = HttpClientErrorException.Unauthorized.class)
    public void doUnauthorizedRequestToTaskService() {
        taskRestClient.getAllTasks();
    }

    @Test(expected = HttpClientErrorException.Unauthorized.class)
    public void doUnauthorizedRequestToUserService() {
        userRestClient.getAllUsers();
    }
}
